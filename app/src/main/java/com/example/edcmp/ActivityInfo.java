package com.example.edcmp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ActivityInfo extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_info3);

        ArrayAdapter<String> infoAdapter =
                new ArrayAdapter<String>(this, R.layout.row_item_info_2, getResources().getStringArray(R.array.information2));

//        String[] values = new String[] { "Android", "iPhone", "WindowsMobile",
//                "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
//                "Linux", "OS/2", "Ubuntu", "Windows7", "Max OS X", "Linux",
//                "OS/2", "Ubuntu", "Windows7", "Max OS X", "Linux", "OS/2",
//                "Android", "iPhone", "WindowsMobile" };
//        ArrayAdapter<String> infoAdapter2 =
//                new ArrayAdapter<String>(this, R.layout.row_item_info_2, values);

        ListView listView = (ListView) findViewById(R.id.lvInfo);
        listView.setAdapter(infoAdapter);
    }


}
