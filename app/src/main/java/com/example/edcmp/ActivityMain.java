package com.example.edcmp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class ActivityMain extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_2);

        ImageButton btn_info = findViewById(R.id.btn_home_info);
        btn_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itn = new Intent(ActivityMain.this, ActivityInfo.class);
                startActivity(itn);
            }
        });

        ImageButton btn_test = findViewById(R.id.btn_home_test);
        btn_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itn = new Intent(ActivityMain.this, ActivityTest.class);
                startActivity(itn);
            }
        });

        Button btn_consouling = findViewById(R.id.btn_consouling);
        btn_consouling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itn = new Intent(ActivityMain.this, ActivityConsouling.class);
                startActivity(itn);

            }
        });
    }
}
