package com.example.edcmp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ActivityTest extends AppCompatActivity {
    String[] soals;
    Integer n = 0;

    TextView tvSoalNo;
    TextView tvSoal;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        soals = getResources().getStringArray(R.array.soal);

        tvSoalNo = findViewById(R.id.tvSoalNo);
        tvSoal = findViewById(R.id.tvSoal);

        Button btnYes = findViewById(R.id.btnYes);
        Button btnNo = findViewById(R.id.btnNo);

        setSoal();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                n = n + 1;
                setSoal();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                n = n + 1;
                setSoal();
            }
        });
    }

    private void setSoal() {
        String prefix = "Question no ";
        tvSoalNo.setText(prefix + Integer.toString(n+1) + "/" + Integer.toString(soals.length));
        tvSoal.setText(soals[n]);
    }
}
